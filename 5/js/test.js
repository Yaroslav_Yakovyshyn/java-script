// Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". 
// Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение",
//  вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.

var doc = {
    title: '_заголовок',
    body: '_тело',
    footer: '_футер',
    data: '_дата',
    app: {
        title_2: '_заголовок',
        body_2: '_тело',
        footer_2: '_футер',
        data_2: '_дата',

        getAppTitle: function () {
            return this.title_2;
        },
        setAppTitle: function (title) {
            this.title_2 = title;
        },
        getAppBody: function(){
            return this.body_2;
        },
        setAppBody: function(body){
            this.body_2 = body;
        },
        getAppFooter: function(){
            return this.footer_2
        },
        setAppfooter: function(foter){
            this.footer_2 = foter;
        },
        getAppData: function(){
            return this.data_2;
        },
        setAppData: function(data){
            this.data_2 = data;
        }
    },
    getTitle: function () {
        return this.title;
    },
    setTitle: function (title) {
        this.title = title;
    },

    getBody: function () {
        return this.body;
    },
    setBody: function (body) {
        this.body = body;
    },
    getFooter: function () {
        return this.footer;
    },
    setFooter: function (footer) {
        this.footer = footer;
    },
    getData: function () {
        return this.data;
    },
    SetData: function (data) {
        this.data = data;
    },




}



console.log(doc.getTitle());
doc.setTitle('Продажа телефонов');
console.log(doc.getTitle());
console.log(doc.getBody());
doc.setBody('Samsung');
console.log(doc.getBody());
console.log(doc.getFooter());
doc.setFooter('tel: 777-777-7');
console.log(doc.getFooter());
console.log(doc.getData());
doc.SetData('31.10.2020');
console.log(doc.getData());
console.log(doc.app.getAppTitle());
doc.app.setAppTitle('Приложение');
console.log(doc.app.getAppTitle());
console.log(doc.app.getAppBody());
doc.app.setAppBody('Yotube');
console.log(doc.app.getAppBody());
console.log(doc.app.getAppFooter());
doc.app.setAppfooter('tel: 777-777-99')
console.log(doc.app.getAppFooter());
console.log(doc.app.getAppData());
doc.app.setAppData('31.10.2020');
console.log(doc.app.getAppData());








