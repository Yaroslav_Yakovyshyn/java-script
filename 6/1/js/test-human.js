// Создаем функцию конструктор Human
function Human(name, age) {
    this.name = name;
    this.age = age;
}
// Создаем экземпляры
const kat = new Human('Katya', 17);
const ph = new Human('Philip', 25);
const yar = new Human('Yaroslav', 20);
const ann = new Human('Anya', 22);




// Создаем масив всех экземпляров
const allHuman = [kat, ph, yar, ann];
// сортируем масив по возростанию
allHuman.sort((prev, next) => prev.age - next.age);
// Выводим масив
console.log(allHuman);

