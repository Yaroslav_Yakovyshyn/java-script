// Разработайте функцию конструктор, которая будет создавать обьект Human(человек),
//  добавте на свое усмотрение свойства и методы в этот обьект.
//  Подумайте какие методы и свойства  следует сделать уровня экземплра, а какие уровня функции-коструктора


//Создаем функцию конструктор Human  
function Human(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
}


// Создаем экземпляр 
const bob = new Human('Bob', 'Jonson', setAge());

// Записываем на прототип функцию которая вычисляет год рождения
Human.prototype.dayOfBirth = function () {
    return 2020 - bob.age;
}
// записываем на прототип функцию которая принимает значение job
Human.prototype.setJob = function () {
    return prompt('Введите кем работает Bob');
}
// записываем на прототип функцию которая принимает значение tel
Human.prototype.setTel = function () {
    return prompt('Введите телефон Bob');
}
// Функция которая принимает Age
function setAge() {
    return +prompt('Введите сколько лет Bob');
}
// Выводим результат
document.write('Имя:' + bob.name + '<br>' + 'Фамилия:' + bob.surname + '<br>' + 'Возраст:' + bob.age + '<br>' + 'Работа:' + bob.setJob() + '<br>' +
    'Телефон:' + bob.setTel() + '<br>' + 'Год рождения:' + bob.dayOfBirth() + '<br>');
